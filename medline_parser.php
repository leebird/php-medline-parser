<?php

class MedlineParser
{
    private $medlinePath;

    private $tiabList;

    private $parsedNum;

    /* successfully downloaded from PubMed */
    private $positiveList;

    private $mapHead = Array("PMID-" => "PMID",
                             "TI  -" => "Title",
                             "AB  -" => "Abstract",
                             "DP  -" => "Date",
                             "AU  -" => "Author",
                             "TA  -" => "Journal");

    function parse()
    {
        if(!file_exists($this->medlinePath))
            return False;

        $medlineContent = file_get_contents($this->medlinePath);
        $medlineContent = trim($medlineContent);
        $medlineContent = str_replace("\r","",$medlineContent);

        // compress each field into one line
        $medlineContent = str_replace("\n     ","",$medlineContent);

        // in meline-format file, pmid blocks are separated by two newline symbols
        $medlineSplitted = explode("\n\n",$medlineContent);

        foreach($medlineSplitted as $medline)
        {
            $medline = trim($medline);
            $lines = explode("\n",$medline);
            $res = Array();
            foreach($lines as $line)
            {
                $headFlag = substr($line,0,5);
                foreach($this->mapHead as $head => $index)
                {
                    if($headFlag == $head)
                        $res[$index][] = trim(substr($line,5));
                }
            }
            $this->tiabList[] = $res;
            $this->positiveList[] = $res["PMID"][0];
        }
        $this->parsedNum = count($this->tiabList);
        return True;
    }

    function get_parsed()
    {
        return $this->tiabList;
    }

    function get_parsed_num()
    {
        return $this->parsedNum;
    }

    function get_pmids()
    {
        return $this->positiveList;
    }

    /* constructor */
    function __construct($medlinePathAug)
    {
        $this->medlinePath = $medlinePathAug;
        $this->positiveList = Array();
        $this->tiabList = Array();
    }
}

?>