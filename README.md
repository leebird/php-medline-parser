# PHP Medline Parser
A PHP class for parsing medline-format files. You can use it to extract information from medline files downloaded from PubMed.