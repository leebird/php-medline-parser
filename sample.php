<?php

require_once('medline_parser.php');

if(count($argv) < 2)
{
    echo "Usage: php sample.php file_path\n";
    exit(0);
}

$parser = new MedlineParser($argv[1]);
$parser->parse();
print_r($parser->get_parsed());

?>